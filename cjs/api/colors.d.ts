export declare enum ColorTypes {
    Grayscale = "Grayscale",
    RGB = "RGB",
    CMYK = "CMYK"
}
export interface Grayscale {
    type: ColorTypes.Grayscale;
    gray: number;
}
export interface RGB {
    type: ColorTypes.RGB;
    red: number;
    green: number;
    blue: number;
}
export interface CMYK {
    type: ColorTypes.CMYK;
    cyan: number;
    magenta: number;
    yellow: number;
    key: number;
}
export declare type Color = Grayscale | RGB | CMYK;
export declare const grayscale: (gray: number) => Grayscale;
export declare const rgb: (red: number, green: number, blue: number) => RGB;
export declare const cmyk: (cyan: number, magenta: number, yellow: number, key: number) => CMYK;
export declare const setFillingColor: (color: Color) => any;
export declare const setStrokingColor: (color: Color) => any;
//# sourceMappingURL=colors.d.ts.map